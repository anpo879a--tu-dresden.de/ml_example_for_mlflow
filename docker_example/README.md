#CIFAR 10 classification

##Prequisities
Singularity container engine

##Set up

specify your credentials:
 
```
export MLFLOW_TRACKING_USERNAME=user
export MLFLOW_TRACKING_PASSWORD=password
```
run container

please cpecify path to container (`cifar10_clf_1c28060.sif`), path to config (by default in the same folder as the readme), path to data (by default in the same folder as the readme)


```
singularity exec --nv -B /data/:/data cifar10_clf_1c28060.sif python -m cifar10_clf.cli -c example_config_cluster.yaml --ExperimentConfig.num_epochs=2 --ExperimentConfig.dropout_p=0.0 --ExperimentConfig.tf32 --ExperimentConfig.model_checkpoint_strategy="last_epoch" --ExperimentConfig.save_model --ExperimentConfig.mlflow_log_system_metrics
```
##Note

if you don't have gpu, specify it in config

import mlflow
import pandas as pd
import numpy as np
from sklearn.linear_model import LinearRegression
from sklearn.datasets import load_iris
from sklearn.model_selection import train_test_split
import argparse
#from mlflow.sklearn import log_model

# Parse command-line arguments
parser = argparse.ArgumentParser()
parser.add_argument("--username", type=str, required=True, help="MLflow username")
parser.add_argument("--password", type=str, required=True, help="MLflow password")
parser.add_argument("--tracking_uri", type=str, required=True, help="IP of the MLflow server")
parser.add_argument("--experiment_name", type=str, required=True, help="Name of the experiment in the MLflow server")
args = parser.parse_args()

os.environ['MLFLOW_TRACKING_USERNAME'] = args.username
os.environ['MLFLOW_TRACKING_PASSWORD'] = args.password

# Load dataset
iris = load_iris()
X = pd.DataFrame(iris.data, columns=iris.feature_names)
y = iris.target

# Split data into training and testing sets
X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

# Train a linear regression model
lr_model = LinearRegression()
lr_model.fit(X_train, y_train)

# Log parameters to MLflow server
mlflow.set_tracking_uri(args.tracking_uri)
mlflow.set_experiment(args.experiment_name)
#mlflow.set_credentials(args.username, args.password)
mlflow.log_param("model_type", "linear_regression")
mlflow.log_param("learning_rate", lr_model.get_params()["learning_rate"])
mlflow.log_param("regularization_strength", lr_model.get_params()["C"])

# Log metrics to MLflow server
mlflow.log_metric("train_r2", lr_model.score(X_train, y_train))
mlflow.log_metric("test_r2", lr_model.score(X_test, y_test))

# Save model to MLflow model registry
mlflow.sklearn.log_model(lr_model, "linear_regression_model")

# Log experiment ID to MLflow server
experiment_id = mlflow.active_run().info.run_id
mlflow.log_metric("experiment_id", experiment_id)

# Close MLflow run
mlflow.end_run()
print ("computations are done, mlflow tracked the experiment")


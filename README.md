# ml_example_for_mlflow

Script do some random computations and track a random metric `foo`. Info necessry for the mlflow server can be specified with args.

## Getting started

install requirements with `pip install -r requirements.txt`

## Run

Args:

`--username` MLflow username

`--password` MLflow password

`--tracking_uri` ip of the mlflow server in the format e.g http://xxx.xx.xxx.xx

`--experiment_name` Name of the experiment in the Mlflow server

To run on scads mlflow server 1, please CHANGE CREDENTIALS e.g :

`python simplest_mlflow_test.py --username max.mastermann@tu-dresden.de --password token_from_reg_app --tracking_uri http://172.26.52.49 --experiment_name mlflow_test_PLS_DELETE`



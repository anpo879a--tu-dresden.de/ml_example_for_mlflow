import os
import argparse
from random import random, randint
from mlflow import log_artifacts, log_param, log_metric
import mlflow

def parse_arguments():
    parser = argparse.ArgumentParser(description="MLflow script with mlflow info as an arguments")
    parser.add_argument("--username", type=str, required=True, help="MLflow username")
    parser.add_argument("--password", type=str, required=True, help="MLflow password")
    parser.add_argument("--tracking_uri", type=str, required=True, help="ip of the mlflow server in the format e.g http://xxx.xx.xxx.xx:5000")
    parser.add_argument("--experiment_name", type=str, required=True, help="Name of the experiment in the Mlflow server")
    
    return parser.parse_args()

def main():
    args = parse_arguments()

    os.environ['MLFLOW_TRACKING_USERNAME'] = args.username
    os.environ['MLFLOW_TRACKING_PASSWORD'] = args.password

    mlflow.set_tracking_uri(args.tracking_uri)
    mlflow.set_experiment(args.experiment_name)

    log_param("param1", randint(0, 100))

    log_metric("foo",random())
    log_metric("foo",random() +1)
    log_metric("foo",random() +2)

    if not os.path.exists("outputs"):
        os.makedirs("outputs")
    with open("outputs/test.txt", "w") as f:
        f.write("hello world")
    log_artifacts("outputs")
    print ("computations are done, mlflow tracked the experiment")

if __name__ == '__main__':
    main()
